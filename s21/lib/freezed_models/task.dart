import 'package:freezed_annotation/freezed_annotation.dart';

// The rest of the contents for user.dart can be founf in user.freezed.dart using the 'part' keyword.
part 'task.freezed.dart';
part 'task.g.dart';

// The @freezed annotation tells the build_runner to create a freezed class named _$User in user.freezed.dart.
@freezed
class Task with _$Task {
    const factory Task({
        required int id,
        required int userId,
        required String description,
        String? imageLocation,
        required int isDone
    }) = _Task;

    factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);
}