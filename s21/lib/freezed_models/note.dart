import 'package:freezed_annotation/freezed_annotation.dart';

// The rest of the contents for user.dart can be founf in user.freezed.dart using the 'part' keyword.
part 'note.freezed.dart';
part 'note.g.dart';

// The @freezed annotation tells the build_runner to create a freezed class named _$User in user.freezed.dart.
@freezed
class Note with _$Note {
    const factory Note({
        required int id,
        required int userId,
        required String title,
        required String description,
    }) = _Note;

    factory Note.fromJson(Map<String, dynamic> json) => _$NoteFromJson(json);
}