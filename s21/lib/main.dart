import './models/user.dart';

void main() {
    // String x = '123';
    // String y = '123';

    // print(x.hashCode);
    // print(y.hashCode);
    // print(x == y);

    // In primitive data types, hash codes are dependent on the value
    // Covert value to hash code

    User userA = User(id: 1, email: 'john@gmail.com');
    User userB = User(id: 1, email: 'john@gmail.com');

    // print(userA.hashCode);
    // print(userB.hashCode);
    // print(userA == userB);

    // To check for object equality, we need to do it like the code below.

    // bool isIdSame = userA.id == userB.id;
    // bool isEmailSame = userA.email == userB.email;
    // bool areObjectsSame = isIdSame && isEmailSame;

    // print(areObjectsSame);


    // In objects, hash codes work differently from primitive types

    // Dart does not check the values of the object
    // but also its unique identifier (through hash codes)

    print(userA.email);
    userA.email= 'john@gotmail.com';
    print(userA);
}