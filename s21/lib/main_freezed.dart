import './freezed_models/user.dart';
import './freezed_models/task.dart';
import './freezed_models/note.dart';

void main() {
    User userA = User(id: 1, email: 'john@gmail.com');
    User userB = User(id: 1, email: 'john@gmail.com');

    // print(userA.hashCode);
    // print(userB.hashCode);
    // print(userA == userB);

// Demonstration of object immutability below.
// Immutability means changes are not allowed.
// Mutable and immutable.
// It ensures that an object will not be changed accidentally.

// Instead of directly changing the object's property,
// The object itself will be changed or re-assigned with new values.
// To achieve this, we use the object.copyWith() method.

    // print(userA.email);
    // userA = userA.copyWith(email: 'john@hotmail.com');
    // print(userA.email);

    var response = {'id': 3, 'email': 'doe@gmail.com'};

    // User userC = User(
    //     id: response['id'] as int, 
    //     email: response['email'] as String
    // );

    // Instead of the code above.

    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());
}