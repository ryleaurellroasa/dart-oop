void main() {
  Building building = new Building(
    name: 'Cayswynn',
    floors: 8,
    address: '134 Timog Avenue, Brgy. Sacred Heart, Quezon City, Metro Manila');

  print(building);
  print(building.name);
  print(building.floors);
  print(building.address);
}

class Building {
  String name; // instance variables
  int floors;
  String address;

  Building({
    required this.name, 
    required this.floors, 
    required this.address
    }) {
    print('A building object has been created.');
  }

  // The class declaration looks like this:
  /*
  class ClassName{
    <fields>
    <constructors>
    <methods>
    <getters-or-setters>
  }
  */
}
