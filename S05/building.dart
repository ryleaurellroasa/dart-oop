// To know when to use late vc. required

// Use "required" when logically, you always require that the variable to have a value.
// A person will always have a first name and last name
// Then, use "required" for both firstName and lastName fields.

// Use "late" when logically, a variable can have a null value.
// A person may or may not have a spouse.
// Then, use "late" for spouseID field.
// Use "late" if you are sure that later on your program , the value will be given to that variable.
// Else,, use the null-enabler instead (?)

class Building {
  String? _name; // hide instance variables using _
  int floors;
  String address;

  Building(this._name, {required this.floors, required this.address}) {
    print('A building object has been created.');
  }
  // The get and set allows for indirect access to class fields.
  String? get Name {
    print('The building\'s name has been retrieved.');
    return this._name;
  }

  // setter
  void set Name(String? name) {
    this._name = name;
    print('The building\'s name has been changed to $name.');
  }

  Map<String, dynamic> getProperties() {
    return {'name': this._name, 'floors': this.floors, 'address': this.address};
  }
}
