void main() {
  // Set<dataType> variableName = {values};

  String firstName = 'John';
  String first_name = 'John';
  // both are valid, but the first naming convention is prefered

  // a SET in Dart is an unordered collection of unique items.
  Set<String> subContractors = {'Sonderhoff', 'Stahlschimdt'};
  subContractors.add('Schweisstechnik');
  subContractors.add('Kreatel');
  subContractors.add('Kunstoffe');
  subContractors.remove('Sonderhoff');
  print(subContractors);
}
