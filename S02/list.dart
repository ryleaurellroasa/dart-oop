void main() {
  // List<dataType> variableName = [values];

  List<int> discountRanges = [20, 40, 60, 80];
  List<String> names = ['John', 'Jane', 'Tom'];
  const List<String> maritalStatus = [
    'single',
    'married',
    'divorced',
    'windowed'
  ];
  print(discountRanges);
  print(names);
  print(discountRanges[0]);
  print(names[0]);
  print(discountRanges.length);
  print(names.length);

  names[0] = 'Jonathan';
  print(names);

  names.add('Mark'); // Add element to the end of the list (push())
  names.insert(
      0, 'Roselle'); // Add element to the begining of the list (unshift())
// These are properties:
  print(names.isEmpty);
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);

// This is a method:
// This method modifies the list alphabetically
  // names.sort();
// This method sorts the list from lowest no. of character to highest
  names.sort((a, b) => a.length.compareTo(b.length));
  print(names.reversed);
}
