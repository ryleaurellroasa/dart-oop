void main() {

  // Map<keyDataType, valueDataType> variableName = {values};

  Map<String, String> address = {
    'specifics': '221B Baker Street',
    'district': 'Marylebone',
    'city': 'London',
    'country': 'United Kingdom'
  };

  address['region'] = 'Europe';

  print(address);
}


// SUMMARY

/* 
List implements the concept of an array
List allows duplicate values while sets do not
List elements can be accessed using a numeric index(ordered) while set elementscannot be accessed using a numeric index(unordered)
Map implements the concept of an object
*/