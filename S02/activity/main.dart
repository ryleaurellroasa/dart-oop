void main() {
  Map<String, dynamic> person = {
    'First Name': 'John',
    'Last Name': 'Smith',
    'Age': 34,
  };

  Set<String> countries = {
    'Canada',
    'Canada',
    'Japan',
    'Philippines',
    'Singapore',
    'Thailand',
    'United Kingdom'
  };

  List<String> processors = [
    'Ryzen 3 3200G',
    'Ryzen 5 3600',
    'Ryzen 7 5800X',
    'Ryzen 9 8950X'
  ];

  print(person.length);
  print(countries.length);
  print(processors.length);
}
