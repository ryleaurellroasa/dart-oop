import 'worker.dart';

void main() {
  // Person personA = new Person();
  Doctor doctor = new Doctor(firstName: 'John', lastName: 'Smith');
  Carpenter worker = new Carpenter();
  print(worker.getType());
}

abstract class Person {
  String getFullName();
}

class Doctor implements Person { // instead of "extends", the class Doctor "implements" the abstract class Person
  String firstName;
  String lastName;

  Doctor({
    required this.firstName, 
    required this.lastName
  });

  String getFullName() {
    return 'Dr. ${this.firstName} ${this.lastName}';
  }
}

class Attorney implements Person {
  String getFullName() {
    return 'Atty. ';
  }
}