void main() {
  Person person = new Person();
  Employee employee = new Employee();

  person.firstName = 'John';
  person.lastName = 'Smith';

  employee.firstName = 'Jonas';
  employee.lastName = 'Westwood';


  print(person.getFullName());
  print(employee.getFullName());
}

class Person {
  late String firstName;
  late String lastName;

  String getFullName() {
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person {}
