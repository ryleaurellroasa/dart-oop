void main() {
  Equipment equipment = new Equipment();
  equipment.name = 'Equipment-001';

  Loader loader = new Loader();
  loader.name = 'Loader-001';
  print(loader.name);
  print(loader.getCategory());

  WheelLoader wheelLoader = new WheelLoader();
  wheelLoader.name = 'WheelLoader-001';
  print(wheelLoader.name);
  print(wheelLoader.getCategory());
}

class Equipment {
  String? name;
}

class Loader extends Equipment {
  String getCategory() {
    return "${this.name} is a loader.";
  }
}

class WheelLoader extends Loader {
  String getCategory() {
    print(super.getCategory());
    return "${this.name} is a wheel loader.";
  }
}
