void main() {
  Equipment equipment = new Equipment();
  equipment.name = 'Equipment-001';

  Loader loader = new Loader();
  loader.name = 'Loader-001';
  print(loader.name);
  print(loader.getCategory());
  loader.moveForward(10);
  loader.moveBackward();

  Car car = new Car();
  car.name = 'Car-001';
  print(car.name);
  print(car.getCategory());
  car.moveForward(10);
  car.moveBackward();
}

class Equipment {
  String? name;
}

class Loader extends Equipment with Movement, Engine {
  String getCategory() {
    return "${this.name} is a loader.";
  }
}

class Car with Movement, Engine {
  String? name;
  String getCategory() {
    return "${this.name} is a car.";
  }
}

mixin Engine {
  void start() {

  }

  void stop() {

  }
}


mixin Movement {
  num? acceleration;

  void moveForward(num acceleration) {
    this.acceleration = acceleration;
    print('The vehicle is moving forwared');
  }

  void moveBackward() {
    print('The vehicle is moving backward');
  }
}
