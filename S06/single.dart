void main() {
  Person person = new Person(
        firstName: 'John', 
        lastName: 'Smith'
      );
  Employee employee = new Employee(
        firstName: 'Jonas', 
        lastName: 'Westwood'
      );

  print(person.getFullName());
  print(employee.getFullName());

}

class Person {
  String firstName;
  String lastName;

  Person({
    required this.firstName, 
    required this.lastName
    });

  String getFullName() {
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person {
  // In spirit the firstName and lastName is inherited by Employee from Person
  // The employee also inherits the getFullName from Person
  // Inheritance then allows us to write less code.
  Employee({
    required String firstName, 
    required String lastName,})
      : super(firstName: firstName, lastName: lastName);
}
