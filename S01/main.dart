// The keyword "void" means the function will not return anything
// The syntax of a function is: <return-type> <function-name>(parameters){}

void main() {
  // var name = "John"; // You cannot longer change the value of the variable with a different data type
  // name = 1.toString();

  String firstName = 'John';
  String? middleName = null;
  String lastName = 'Smith';
  int age = 31; // for numbers without decimal points
  double height = 172.45; // for numbers with decimal points
  num weight = 64.32; // can accept number with or without decimal points
  bool isRegistered = false; // true or false
  List<num> grades = [98.2, 89, 87.88, 91.2]; // similar to an array

  Map<String, dynamic> personA = {'name': 'Brandon', 'batch': 213};

// Alternative syntax for declaring object
  Map<String, dynamic> personB = new Map();
  personB['name'] = 'Juan';
  personB['batch'] = 89;

// With FINAL, once a vlaue has been set, it cannot be changed.
  final DateTime now = DateTime.now(); // Get the current date/time of system
// With CONST, an identifier must have a corresponding decaration of value
  const String companyAcronym = 'FFUF';
  // companyAcronym = 'FFUF';

  // print(firstName +
  //     ' ' +
  //     lastName); // The 'Hello' is cancatenated with the the name variable.
  print('Full name: $firstName $lastName');
  print('Age: ' + age.toString());
  print('Height: ' + height.toString());
  print('Weight: ' + weight.toString());
  print('Registered: ' + isRegistered.toString());
  print('Grades: ' + grades.toString());
  print('Current Date/Time: ' + now.toString());

  // To access the value of the object
  print(personB['name']);
}
