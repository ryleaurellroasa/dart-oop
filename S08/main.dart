void main() {
  List<Equipment> equipment = [];

  Bulldozer bulldozer = new Bulldozer(
    name: 'Caterpillar D10', 
    bladeType: 'U blade'
  );

  TowerCrane towerCrane = new TowerCrane(
    name: '370 EC-B 12 Fibre', 
    hookRadius: 78, 
    capacity: 12000
  );

  Loader loader = new Loader(
    name: 'Volvo L60H', 
    type: 'wheel loader', 
    tippingLoad: 16530
  );

  equipment.add(bulldozer);
  equipment.add(towerCrane);
  equipment.add(loader);
  equipment.forEach((equipment) { 
    print(equipment.describe());
  });
}

abstract class Equipment {
  String describe();
}

class Bulldozer implements Equipment {
  String name;
  String bladeType;

  Bulldozer({
    required this.name, 
    required this.bladeType
  });

  String describe() {
    return 'The bulldozer $name has a $bladeType.';
  }
}

class TowerCrane implements Equipment {
  String name;
  num hookRadius;
  num capacity;

  TowerCrane({
    required this.name, 
    required this.hookRadius, 
    required this.capacity
  });

  String describe() {
    return 'The tower crane $name has a radius of ${hookRadius.toString()} and a max capacity of ${capacity.toString()}.';
  }
}

class Loader implements Equipment {
  String name;
  String type;
  num tippingLoad;

  Loader({
    required this.name, 
    required this.type, 
    required this.tippingLoad
  });
  
  String describe() {
    return 'The loader $name is a $type and has a tipping load of ${tippingLoad.toString()} lbs.';
  }
}