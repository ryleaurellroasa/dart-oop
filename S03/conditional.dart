void main() {
  print(showServiceItems('robotics'));
  print(determineTyphoonIntensity(67));
  print(selectSector(3));
  print(isUnderAge(18));
}

List<String>? showServiceItems(String category) {
  if (category == 'apps') {
    return ['native', 'android', 'ios', 'web'];
  } else if (category == 'cloud') {
    return ['azure', 'microservices'];
  } else if (category == 'robotics') {
    return ['sensors', 'fleet-tracking', 'realtime-communication'];
  } else {
    return [];
  }
}

String determineTyphoonIntensity(int windSpeed) {
  if (windSpeed < 30) {
    return 'Not a typhoon yet.';
  } else if (windSpeed <= 61) {
    return 'Tropical depression detected.';
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return 'Tropical Storm detected';
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return 'Severe tropical storm detected';
  } else {
    return 'Typhoon detected.';
  }
}

String selectSector(int sectorId) {
  String name;
  switch (sectorId) {
    case 1:
      name = 'Craft';
      break;
    case 2:
      name = 'Assembly';
      break;
    case 3:
      name = 'Building Operations';
      break;
    default:
      name = sectorId.toString() + 'is not a sector';
  }
  return name;
}

bool isUnderAge(age) {
  return age < 18; // ternary operators
}
