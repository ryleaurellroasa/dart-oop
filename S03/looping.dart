void main() {
  // whileLoop();
  // doWhile();
  // forLoop();
  // forInLoop();
  modifiedForLoop();
}

void whileLoop() {
  int count = 5;
  while (count != 0) {
    print(count);
    count--;
  }
}

void doWhile() {
  int count = 20;

  do {
    print(count);
    count--;
  } while (count > 0);
} // execute line first before looping

void forLoop() {
  // for(initializer, condition, iterator)
  for (int count = 0; count <= 20; count++) {
    print(count);
  }
}

void forInLoop() { // iterate through each element
  List<Map<String, dynamic>> persons = [
    {'name': 'Brandon'},
    {'name': 'John'},
    {'name': 'Arthur'},
  ]; 

  for (var person in persons) {
    print(person['name']);
  }
}

// Branching statements
void modifiedForLoop() {
  for (int count = 0; count <= 20; count++) {
    if (count > 10) {
      break; // stops the loop
    }
    if (count % 2 == 0) {
      continue; // proceed to the next loop if condition is met
    } else {
      print(count);
    }
  }
}
