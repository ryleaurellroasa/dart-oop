void main() {
  // Assignement Operator:
  int x = 1397;
  int y = 7831;

  // Arithmetic Operators:
  num sum = x + y;
  num difference = x - y;
  num products = x * y;
  num quotient = x / y;
  num remainder = x % y;
  num output = (x * y) - (x / y + x);

  // Relational Operators:
  bool isGreaterThan = x > y;
  bool isLessThan = x < y;
  bool isGreaterOrEqual = x >= y;
  bool isLesserOrEqual = x <= y;

  bool isEqual = x == y;
  bool isNotEqual = x != y;

  // Logical Operators:
  bool isLegalAge = true;
  bool isRegistered = false;
  bool areAllRequirementsMet = isLegalAge && isRegistered;
  bool areSomeRequrementsMet = isLegalAge || isRegistered;
  bool isNotRegistered = !isRegistered;

  // Increment & decrement operators
  print(x++);
  print(x--);
}
