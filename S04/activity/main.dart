/*
Using a function, get the total price of prices [45, 34.2, 176.9, 32.2] and optionally get its discounted price.

Use a loop to get the total price.

288.3 (no discount)
230.64 (20% discount)
172.98 (40% discount)
115.32 (60% discount)
57.66 (80% discount)
*/

void main() {
  List<num> prices = [45, 34.2, 176.9, 32.2];
  num totalPrice = getTotal(prices);
  print(displayDiscounted(totalPrice, 0, 'no'));
  print(displayDiscounted(totalPrice, 20, '20%'));
  print(displayDiscounted(totalPrice, 40, '40%'));
  print(displayDiscounted(totalPrice, 60, '60%'));
  print(displayDiscounted(totalPrice, 80, '80%'));
}

String displayDiscounted(num totalPrice, num discount, String discountLabel) {
  return 
      (getDiscount(discount)(totalPrice)).toStringAsFixed(2) + ' ($discountLabel discount)';
}

num getTotal(List prices) {
  num totalPrice = 0;
  prices.forEach((price) => totalPrice += price);
  return totalPrice;
}

Function getDiscount(num percentage) {
  return (num amount) {
    return amount > 0 ? amount - (amount * percentage / 100) : amount;
  };
}
