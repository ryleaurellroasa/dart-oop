void main() {
  List<num> prices = [45, 34.2, 176.9, 32.2];
  num totalPrice = getTotalPrice(prices);
  print(getDiscountedPrice(0)(totalPrice));
  print(getDiscountedPrice(20)(totalPrice));
  print(getDiscountedPrice(40)(totalPrice));
  print(getDiscountedPrice(60)(totalPrice));
  print(getDiscountedPrice(80)(totalPrice));
}

num getTotalPrice(List<num> prices) => prices.reduce((a, b) => a + b);

Function getDiscountedPrice(num percentage) => (num amount) => amount - (amount * percentage / 100);
