void main() {
  Function discountBy25 = getDiscount(25);
  // The discountBy25 is then considered as a CLOSURE.
  // The CLOSURE has access to variablesin its lexical scope.
  Function discountBy50 = getDiscount(50);
  print(discountBy25(1400));
  print(discountBy50(1400));
  
  print(getDiscount(25)(1400));
  print(getDiscount(50)(1400));
}

Function getDiscount(num percentage) {
  // When the getDicount is used and the funtion is returned, the value of 'percentage' parameter is retained
  // That is called lexical scope.
  return (num amount) {
    return amount * percentage / 100;
  };
}
