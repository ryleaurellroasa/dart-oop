// The main() function is the entry point  of Dart applications
void main() {
  // print(getCompanyName());
  // print(getYearEstablished());
  // print(hasOnlineClasses());
  // print(getCoordinates());
  // print(combineAddress(
  //     '134 Timog Ave', 'Brgy. Sacred Heart', 'Quezon City', 'Metro Manila'));
  // print(combineName('John', 'Smith'));
  // print(combineName('John', 'Smith', isLastNameFirst: true));
  // print(combineName('John', 'Smith', isLastNameFirst: false));

  List<String> persons = ['John Doe', 'Jane Doe'];
  List<String> students = ['Nicholas Rush', 'James Holden'];

// Anonymous Functions:
  // persons.forEach((String person) {
  //   print(person);
  // });
  // students.forEach((String person) {
  //   print(person);
  // });

// Function as objects/variable and used as an argument
// printName(value) is function call/execution/invocation
// printName is reference to the given function
  persons.forEach(printName);
  students.forEach(printName);

  print(isunderAge(19));
}

void printName(String name) {
  print(name);
}

// Optional named parameters
// These are parameters added after the required ones.
// These parameters are added inside a curly bracket
// Use default value for optional named parameters
String combineName(String firstName, String lastName,
    {bool isLastNameFirst = false}) {
  return (isLastNameFirst) ? '$lastName $firstName' : '$firstName $lastName';
}

String combineAddress(
    String specifics, String barangay, String city, String province) {
  return '$specifics, $barangay, $city, $province';
}

String getCompanyName() {
  return 'FFUF';
}

int getYearEstablished() {
  return 2021;
}

bool hasOnlineClasses() {
  return true;
}

// The initial isUnderAge functionn can be changed into a lambda (arrow) function.
// A lambda function is a shortcut function for returning values from simple operations
// The syntax of a lambda function is:
// <return-type> <function-name>(parameters) => expression;
bool isunderAge(int age) => age < 18;

// bool isUnderAge(int age) {
//   return (age < 18);
// }

Map<String, double> getCoordinates() {
  return {'latitude': 14.632702, 'longitude': 121.043716};
}

// The following syntax is followed when creating a function
/*
      <return-type> <function-name>(param-data-type, parameterA, param-data-type, parameterB) {
        // code logic inside a function
        return <return-value>
      }
*/

// Arguments are the values being passed to the function
// Parameters are the values being received by the function

// x (parameter) = 5 (argument)
