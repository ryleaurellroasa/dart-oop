import 'package:flutter/material.dart';
import './body_questions.dart';
import './body_answers.dart';

void main() {
    runApp(App());
}

class App extends StatefulWidget {
    @override
    AppState createState() => AppState();
}

class AppState extends State<App> {

    int questionIdx = 0;
    bool showAnswers = false;

    final questions = [
        {
            'question': 'What is the nature of your business needs?',
            'options': [
                'Time Tracking', 
                'Asset Management', 
                'Issue Tracking'
            ] 
        },
        {
            'question': 'What is the expected size of the user base?', 
            'options': [
                'Less Than 1,000', 
                'Less Than 10,000', 
                'More Than 10,000'
            ]
        },
        {
            'question': 'In which region would the majority of the user base be?', 
            'options': [
                'Asia', 
                'Europe', 
                'Americas', 
                'Africa', 
                'Middle East'
            ]
        },
        {
            'question': 'What is the expected project duration?',
            'options': [
                'Less than 3 months', 
                '3-6 months', 
                '6-9 months', 
                '9-12 months', 
                'More than 12 months'
            ]
        },
    ];

    var answers = [];

    void nextQuestion(String? answer) {
        answers.add({
            'question': questions[questionIdx]['question'],
            'answer': (answer == null) ? '' : answer
        });
        
        (questionIdx < questions.length - 1) 
        ? setState(() => questionIdx++)
        : setState(() => showAnswers = true);    
    }

@override
    Widget build(BuildContext context) {
    
        var bodyQuestions = BodyQuestions(
            questions: questions, 
            questionIdx: questionIdx, 
            nextQuestion: nextQuestion
        );
        var bodyAnswers = BodyAnswers(answers: answers);

        Scaffold homepage = Scaffold(
            appBar: AppBar(
                backgroundColor: Colors.blueGrey[700],
                brightness: Brightness.dark,
                title: Text('Homepage'),
            ),
            body: (showAnswers) ? bodyAnswers : bodyQuestions,
        );

        return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: homepage,
        );
    }
}

// The invisible widgets are Container and Scaffold.
// The visible widgets are AppBar and Text.

// To specify margin or padding spacing, we can use EdgeInsets.
// The value for spacing are in multiple of 8 (e.g. 16, 24, 32).
// To add spacing on all sides, use EdgeInsets.all().
// To add spacing on certain sides, use EdgeInsets.only(direction: value),
// To put colors on a container, the decoration: BoxDecoration(color: Colors.<color>) can be used.

// In a Column widget, the main axis alignment is vertical (from top to bottom).
// To change the horizontal alignment of widgets in a column, we must use crossAxisAlignment.
// For example, if we want to place column widgets to the horizontal left, we set CrossAxisAlignment.start.
// In the context of the Column, we can consider the column as vertical, and its cross axis as horizontal.

// In Flutter, width = double.infinity is the equivalent to 100% in CSS


// The scaffold widget provides basic design layout structure.
// The scaffold can be given UI elements such as an app bar.