import 'package:flutter/material.dart';
import './answer_button.dart';

class BodyQuestions extends StatelessWidget {
    final questions;
    final int questionIdx;
    final Function nextQuestion;

    BodyQuestions({
        required this.questions,
        required this.questionIdx,
        required this.nextQuestion
    });

    @override
    Widget build(BuildContext context) {
        var options = questions[questionIdx]['options'] as List<String>;
        var answerOptions = options.map((String option) {
            return AnswerButton(text: option, nextQuestion: nextQuestion);
        });

    Text questionLabel = Text(
        questions[questionIdx]['question'].toString(),
        style: TextStyle(
        fontSize: 20.0,
        ),
    );

    return Container(
        width: double.infinity,
        padding: EdgeInsets.all(16.0),
        child: Column(
            children: [
                questionLabel,
                ...answerOptions,
                Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 30),
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.blueGrey[900]),
                        child: Text('Skip question'),
                        onPressed: () => nextQuestion(null),
                    ),
                ),
            ],
        ), 
    );
    }
}